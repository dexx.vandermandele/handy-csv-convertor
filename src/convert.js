const fs = require('fs');
const parseCsv = require('./parseCsv');

function humanReadablizeVotes(votes) {
  if (votes > 999999) {
    const millions = Math.round(votes / 100000) / 10;
    return `${millions}M`;
  }

  if (votes > 999) {
    const thousands = Math.round(votes / 100) / 10;
    return `${thousands}K`;
  }

  return votes;
}

function convert(ratings) {
  return ratings
    .map(line => `${line.taskOid},${Math.round(line.rating * 10) / 10},${humanReadablizeVotes(line.votes)}`)
    .join('\n');
}

const newFilePath = './hasBeenConverted.csv';
fs.writeFileSync(newFilePath, convert(parseCsv()));
console.log(`CSV successfully converted to ${newFilePath}`);
