const parseCsv = require('./parseCsv');

function check(ratings) {
  const duplicates = [];
  while (ratings.length > 0) {
    let currentRating = ratings.pop();
    if (ratings.some(queryRating => currentRating.taskOid == queryRating.taskOid)) {
      duplicates.push(currentRating);
    }
  }

  if (duplicates.length > 0) {
    return duplicates.map(rating => rating.taskOid).join(', ');
  }

  return 'No duplicate taskOids found';
}

console.log(check(parseCsv()));
