const fs = require('fs');

function convertToNumber(value) {
  const numValue = parseInt(value);
  if (isNaN(numValue)) {
    throw new Error(`${value} in ${lineString} cannot be cast to a number`);
  }
  return numValue;
}

function parseRating(lineString) {
  const lineValues = lineString.split(',');

  return {
    taskOid: convertToNumber(lineValues[0]),
    votes: convertToNumber(lineValues[1]),
    rating: lineValues[2],
  };
}

function parseCsv() {
  return fs
    .readFileSync('./toBeConverted.csv', 'utf-8')                    // Read file
    .replace(/"(\d*,\d*)+"/g, input => input.replace(/[^0-9]/g, ''))  // Sanitize string
    .split('\r\n')                                                    // Split into lines
    .splice(1)                                                        // Remove headers
    .map(parseRating);                                                // Map to objects
}

module.exports = parseCsv;
